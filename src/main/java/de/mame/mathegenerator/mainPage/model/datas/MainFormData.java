package de.mame.mathegenerator.mainPage.model.datas;

public class MainFormData {
    Integer NumberOfExercises;

    public MainFormData() {
        NumberOfExercises = 10;
    }

    public Integer getNumberOfExercises() {
        return NumberOfExercises;
    }
    public void setNumberOfExercises(Integer numberOfExercises) {
        NumberOfExercises = numberOfExercises;
    }


}
