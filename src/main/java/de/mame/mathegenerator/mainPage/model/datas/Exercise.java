package de.mame.mathegenerator.mainPage.model.datas;

public class Exercise {
    String value;

    public String getValue() {
        return value;
    }
    public void setValue(String value) {
        this.value = value;
    }

    public Exercise() {
    }
    public Exercise(String value) {
        this.value = value;
    }


}
